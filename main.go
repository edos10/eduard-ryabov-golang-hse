package main

import (
	"fmt"
	"hash/fnv"
	"math/rand"
)

func hashGenId(title string) uint32 {
	hashTitle := fnv.New32()
	fmt.Println("AAAAAAAAA")
	hashTitle.Write([]byte(title))
	return hashTitle.Sum32()
}

func randomGenId(title string) uint32 {
	maxVal := 100000
	res := uint32(rand.Intn(maxVal))
	return res
}

func incrementGenId(title string) uint32 {
	return 0
}

type Book struct {
	Title      string
	Author     string
	Pages      []string
	CountPages int
}

type Storage struct {
	generatorIdFunc func(string) uint32
	IdToBook        map[uint32]Book
	TitleToId       map[string]uint32
	TitlesSlice     []Book
	Search          func(string) (Book, error)
	Add             func(Book) error
}

func InitStorage() *Storage {
	newStorage := Storage{IdToBook: make(map[uint32]Book), TitleToId: make(map[string]uint32)}
	return &newStorage
}

func (stg Storage) SearchBookOnMap(title string) (Book, error) {
	foundId, ok := stg.TitleToId[title]
	if !ok {
		return Book{}, fmt.Errorf("Такой книги не существует.")
	}
	foundBook, _ := stg.IdToBook[foundId]
	return foundBook, nil
}

func (stg Storage) SearchBookOnSlice(title string) (Book, error) {
	for _, value := range stg.TitlesSlice {
		if value.Title == title {
			return value, nil
		}
	}
	return Book{}, fmt.Errorf("Такой книги не существует.")
}

func (stg Storage) AddBookOnMap(newBook Book) error {
	fmt.Println(newBook.Author, newBook.Title)
	_, ok := stg.TitleToId[newBook.Title]
	if ok {
		return fmt.Errorf("Эта книга уже существует.")
	}
	fmt.Println(stg.generatorIdFunc)
	newId := stg.generatorIdFunc(newBook.Title)
	fmt.Println(newId)
	stg.TitleToId[newBook.Title] = newId
	stg.IdToBook[newId] = newBook
	return nil
}

func (stg Storage) AddBookOnSlice(newBook Book) error {
	_, err := stg.SearchBookOnSlice(newBook.Title)
	if err != nil {
		return fmt.Errorf("Эта книга уже существует.")
	}
	stg.TitlesSlice = append(stg.TitlesSlice, newBook)
	return nil
}

type Library struct {
	Storage
}

func main() {
	fmt.Println("Введите, пожалуйста, тип хранения данных в библиотеке: 1 - slice, 2 - map, цифру 1 или 2:")
	var input int
	fmt.Scanln(&input)
	if input != 1 && input != 2 {
		fmt.Println("Попробуйте еще раз.")
		return
	}
	newStorage := InitStorage()
	if input == 1 {
		newStorage.Add = newStorage.AddBookOnSlice
		newStorage.Search = newStorage.SearchBookOnSlice
	} else {
		newStorage.Add = newStorage.AddBookOnMap
		newStorage.Search = newStorage.SearchBookOnMap
	}
	newStorage.generatorIdFunc = hashGenId
	library := Library{Storage: *newStorage}
	book1 := Book{Title: "A", Author: "A.B.C"}
	fmt.Println(newStorage.generatorIdFunc)
	library.Add(book1)
	book2 := Book{Title: "B", Author: "D.R."}
	library.Add(book2)
	book3 := Book{Title: "C", Author: "A.R."}
	library.Add(book3)
	book4 := Book{Title: "D", Author: "A.R."}
	library.Add(book4)
	book5 := Book{Title: "E", Author: "E.R."}
	library.Add(book5)
	book, err := library.Search("A")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("Книга найдена: %s, Автор: %s\n", book.Title, book.Author)
	}
	book, err = library.Search("E")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("Книга найдена: %s, Автор: %s\n", book.Title, book.Author)
	}

}
